import {VisitDentist} from "../classes/VisitDentist.js";
import {VisitCardiologist} from "../classes/VisitCardiologist.js";
import {VisitTherapist} from "../classes/VisitTherapist.js";
import {getCards} from "./api.js";

export const checkToken = () => {
    const entranceBtn = document.getElementById("entranceBtn");
    const creatBtn = document.getElementById("creatBtn");
    const token = localStorage.getItem("token");
    const root = document.getElementById("root")
    const emptyMessage = "No item have been added"
    if (token) {
        entranceBtn.style.display = "none";
        creatBtn.style.display = "block";
        getCards().then(arr => {
            if (!arr.length) {

            } else {
                root.innerHTML = ""
                arr.forEach(item => {

                    rendercardOrInput(item.doctor, "renderCard", item)

                })
                checkEmptyColection()
            }


        });
    } else {
        entranceBtn.style.display = "block";
        creatBtn.style.display = "none";

    }
}
export const getRightData = (value) => {
    return value ? value : ""
}

export function rendercardOrInput(value, method, object) {
console.log(value, method, object);
    let currentObj = null
    switch (value) {

        case "dentist" :
            currentObj = new VisitDentist()
            if (object) {
                const {dateLastVisit, description, doctor, fullName, id, purpose, urgency} = object
                currentObj = new VisitDentist(id, doctor, fullName, purpose, description, urgency, dateLastVisit)
            }

            return currentObj[method]()
        case "cardiologist" :
            currentObj = new VisitCardiologist()
            if (object) {
                const {age, doctor, fullName, id, description, purpose, urgency, massIndex, pressure, ill} = object

                currentObj = new VisitCardiologist(id, doctor, fullName, purpose, description, urgency, pressure, massIndex, ill, age)
            }

            return currentObj[method]()
        case "therapist" :
            currentObj = new VisitTherapist()
            if (object) {
                const {age, doctor, fullName, id, description, purpose, urgency} = object

                currentObj = new VisitTherapist(id, doctor, fullName, purpose, age, description, urgency)
            }

            return currentObj[method]()
        default :
            return ""
    }
}
export function checkEmptyColection (){
    const cardAll = document.querySelectorAll(".myCard")
    const emptyMessage = document.querySelector(".emptyMessage")
    if(cardAll.length){
        emptyMessage.style.display = "none"
    } else {
        emptyMessage.style.display = "block"
    }
}