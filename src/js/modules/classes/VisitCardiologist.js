import {Visit} from "./Visit.js";
import { getRightData } from "../helpers/otherFunction.js";


export class VisitCardiologist extends Visit {
  constructor (id, doctor, fullName, purpose, description, urgency, pressure, massIndex, ill, age) {
      super(id, doctor, fullName, purpose, description, urgency)
      this.pressure = pressure
      this.massIndex = massIndex
      this.ill = ill
      this.age = age
  }
  renderCard(){
      const cardiologContent =
      `<div class = "info_content">
      <h3 class = "text">обычное давление:</h3>
      <p>${this.pressure}</p>
      </div>
      <div class = "info_content">
      <h3 class = "text">индекс массы тела :</h3>
      <p>${this.massIndex}</p>
      </div>
      <div class = "info_content">
      <h3 class = "text">Перенесенные заболевания сердечно-сосудистой системы :</h3>
      <p>${this.ill}</p>
      </div>
      <div class = "info_content">
      <h3 class = "text">Возраст :</h3>
      <p>${this.age}</p>
      </div>`
      super.renderCard(cardiologContent)
  }
  renderInputs () {
   return `${super.renderInputs()}
    
    
    
    
   <div class="mb-3">
<label  class="form-label">обычное давление</label>
<input name="pressure" type="text" class="form-control" placeholder="Введите обычное давление" value="${getRightData (this.pressure)}">
</div>
<div class="mb-3">
<label  class="form-label">индекс массы тела</label>
<input name="massIndex" type="text" class="form-control" placeholder="Введите индекс массы тела" value="${getRightData (this.massIndex)}">
</div>


<div class="mb-3">
  <label  class="form-label">Перенесенные заболевания сердечно-сосудистой системы</label>
  <input name="ill" type="text" class="form-control" placeholder="Введите перенесенные заболевания сердечно-сосудистой системы" value="${getRightData (this.ill)}">
</div>
<div class="mb-3">
  <label  class="form-label">Возраст</label>
  <input name="age" type="text" class="form-control" placeholder="Введите возраст" value="${getRightData (this.age)}">
</div> `




}
}
