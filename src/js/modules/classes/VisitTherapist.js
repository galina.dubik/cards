import {Visit} from "./Visit.js";
import { getRightData } from "../helpers/otherFunction.js";

export class VisitTherapist extends Visit {
    constructor (id, doctor, fullName, purpose, age,  description, urgency) {
        super (id, doctor, fullName, purpose, description, urgency)
        this.age = age;
    }
    renderCard() {
        const terapistContent =
            `<div class = "info_content">
        <h3 class = "text"> Ваш возраст :</h3>
        <p>${this.age}</p>
        </div>
       
        
        </div>`
        super.renderCard(terapistContent)

    }
    renderInputs () {
       return `${super.renderInputs()}
        
        
        

<div class="mb-3">
  <label  class="form-label">Возраст</label>
  <input name="age" type="text" class="form-control" placeholder="Введите возраст" value="${getRightData (this.age)}">
</div> `




    }
}