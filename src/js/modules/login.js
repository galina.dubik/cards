import Modal from "./classes/Modal.js"
import Form from "./classes/Form.js"
import {getToken} from "./helpers/api.js";
import {checkToken} from "./helpers/otherFunction.js";


const entranceBtn=document.getElementById("entranceBtn")
entranceBtn.addEventListener("click",login )
checkToken ();
function login () {
    const form =new Form();
    const modal =new Modal(form.renderLogin(),"Войти");
    modal.render();
    const formLogin = document.getElementById("loginForm");
    formLogin.addEventListener("submit", (e)=> {
        e.preventDefault();
        const emailInput = document.getElementById("emailInput");
        const passwordInput = document.getElementById("passwordInput");
        getToken (emailInput.value, passwordInput.value).then(token => {
            if (!token.includes("Validation")){
                localStorage.setItem("token", token)
                checkToken ();
                modal.delete();
            }
        });
    })
}

